package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Artikal;
import model.Kategorija;
import util.ConnectionManager;


public class ArtikalDAO {

	public List<Artikal> getAllArtikli() {
		String query = "select id, naziv, marka, opis, tip, kategorija, cena from artikal";
		Statement stmt;
		List<Artikal> retVal = null;
		try {
			stmt = ConnectionManager.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery(query);
			retVal = new ArrayList<Artikal>();
			while (rset.next()) {
				int id = rset.getInt(1);
				String naziv = rset.getString(2);
				String marka = rset.getString(3);
				String opis = rset.getString(4);
				String tip = rset.getString(5);
				int kategorijaId = rset.getInt(6);
				double cena = Double.parseDouble(rset.getString(7));
				
				KategorijaDAO katdao = new KategorijaDAO();
				Kategorija kategorija = katdao.getKategorijaId(kategorijaId);
			
				
				retVal.add(new Artikal(id, naziv, marka, opis, tip, kategorija, cena));
			}
			rset.close();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}
	
	public boolean deleteItem(int id) {
		boolean retVal = false;
		try {
			Connection conn = ConnectionManager.getConnection();

			String selectSQL = "DELETE FROM artikal WHERE id = ?";
			PreparedStatement preparedStatement = conn
					.prepareStatement(selectSQL);
			preparedStatement.setInt(1, id);
			if (preparedStatement.executeUpdate() == 1)
				retVal = true;
			preparedStatement.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retVal;
	}
	
}
