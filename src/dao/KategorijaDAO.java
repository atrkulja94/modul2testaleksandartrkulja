package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.Kategorija;
import util.ConnectionManager;


public class KategorijaDAO {

	
	public Kategorija getKategorijaId(int id) {
		Kategorija kategorija = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			String selectSQL = "SELECT naziv FROM kategorija WHERE id = ?";
			PreparedStatement preparedStatement = conn
					.prepareStatement(selectSQL);
			preparedStatement.setInt(1, id);
			ResultSet rset = preparedStatement.executeQuery();

			if (rset.next()) {
				String naziv = rset.getString(1);

			kategorija = new Kategorija(id, naziv);
			}
			rset.close();
			preparedStatement.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return kategorija;
	}
}
