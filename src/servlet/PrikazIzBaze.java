package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ArtikalDAO;
import model.Artikal;

/**
 * Servlet implementation class PrikazIzBaze
 */
public class PrikazIzBaze extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrikazIzBaze() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArtikalDAO artdao = new ArtikalDAO();
		List<Artikal> artikli = new ArrayList<>();
		artikli = artdao.getAllArtikli();
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		out.println("</head>");
		out.println("<body>");

		out.println("<table border=\"1\"><tr bgcolor=\"lightgrey\"> <tr>\r\n" + 
				"            <th>Naziv</th>\r\n" + 
				"            <th>Cena</th>\r\n" + 
				"            <th>Marka</th>\r\n" + 
				"            <th>Kategorija</th>\r\n" + 
				"            <th>Tip</th>\r\n" + 
				"            <th>Opis</th><th>&nbsp;</th>\r\n" + 
				"        </tr>");
		
		for (Artikal d : artikli) {
			out.println("<tr>");
			out.println("<td>" + d.getNaziv() + "</td>");
			out.println("<td>" + d.getCena() + "</td>");
			out.println("<td>" + d.getMarka()+ "</td>");
			out.println("<td>" + d.getKategorija().getNaziv() + "</td>");
			out.println("<td>" + d.getTip() + "</td>");
			out.println("<td>" + d.getOpis() + "</td>");
			out.println("<td><form action=\"DeleteServlet\" method=\"post\">"
					+ "<input type=\"hidden\" name=\"id\" value=\"" + d.getId() + "\">"
							+ "<input type=\"submit\" value=\"delete\"></form></td>");
			out.println("</tr>");
		}
		out.println("</table>");

		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
