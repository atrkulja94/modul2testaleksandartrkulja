package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.ArtikalDAO;
import model.Artikal;


/**
 * Servlet implementation class AjaxPrikaz
 */
public class AjaxPrikaz extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxPrikaz() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArtikalDAO artdao = new ArtikalDAO();
		List<Artikal> artikli = new ArrayList<>();
		artikli = artdao.getAllArtikli();
		
		ObjectMapper mapper = new ObjectMapper();
		String sArtikli = mapper.writeValueAsString(artikli);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		out.write(sArtikli);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
