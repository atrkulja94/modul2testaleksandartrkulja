package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Artikal;

import model.Kategorija;


/**
 * Servlet implementation class FiltriranaLista
 */
public class FiltriranaLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FiltriranaLista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ArrayList<Artikal> artiklii = new ArrayList<>();
		Artikal a1 = new Artikal("dasasfd", "nike", "nestoo", "�enski", new Kategorija("majica"), 2000);
		Artikal a2 = new Artikal("dasasfd", "nsdafe", "glupost", "�enski", new Kategorija("duks"), 2222);
		Artikal a3 = new Artikal("fsdafdsaf", "nsssske", "nafsdao", "muski", new Kategorija("majica"), 2030);
		Artikal a4 = new Artikal("dasa232222sfd", "ddddd", "glsdfaaat", "muski", new Kategorija("duks"), 5522);
		Artikal a5 = new Artikal("drrrrrrrr3fd", "fff", "nessafoo", "�enski", new Kategorija("majica"), 550);
		Artikal a6 = new Artikal("yyyyyyysfd", "ffffffff", "glupost", "�enski", new Kategorija("duks"), 26662);
		artiklii.add(a1);
		artiklii.add(a2);
		artiklii.add(a3);
		artiklii.add(a4);
		artiklii.add(a5);
		artiklii.add(a6);
		getServletContext().setAttribute("artiklii", artiklii);
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}
		
		List<Artikal> sviArtikli = (List<Artikal>) getServletContext().getAttribute("artiklii");
		ArrayList<Artikal> filtrirani = new ArrayList<>();
		
		for(Artikal a : sviArtikli) {
			if(a.getTip().equals("�enski")) {
				filtrirani.add(a);
			}
		}
		
		request.getSession().setAttribute("filtrirani", filtrirani);
		response.sendRedirect("FiltriraniArtikli.jsp");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
