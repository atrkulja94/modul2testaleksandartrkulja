package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Artikal;

import model.Kategorija;


/**
 * Servlet implementation class TabelarniPrikaz
 */
public class TabelarniPrikaz extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ArrayList<Artikal> artikli = new ArrayList<>();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TabelarniPrikaz() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		Artikal a1 = new Artikal("dasasfd", "nike", "nestoo", "ženski", new Kategorija("majica"), 2000);
		Artikal a2 = new Artikal("dasasfd", "nike", "glupost", "ženski", new Kategorija("duks"), 2222);
		
		artikli.add(a1);
		artikli.add(a2);
		getServletContext().setAttribute("artikli", artikli);
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String naziv = request.getParameter("naziv");
		double cena = Double.parseDouble(request.getParameter("cena"));
		String marka = request.getParameter("marka");
		String tip = request.getParameter("tip");
		String opis = request.getParameter("opis");
		String kategorija = request.getParameter("kategorija");

		Artikal artikal = new Artikal(naziv, marka, opis, tip, new Kategorija(kategorija), cena);
		artikli.add(artikal);
		getServletContext().getAttribute("artikli");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		out.println("</head>");
		out.println("<body>");

		out.println("<table border=\"1\"><tr bgcolor=\"lightgrey\"> <tr>\r\n" + 
				"            <th>Naziv</th>\r\n" + 
				"            <th>Cena</th>\r\n" + 
				"            <th>Marka</th>\r\n" + 
				"            <th>Kategorija</th>\r\n" + 
				"            <th>Tip</th>\r\n" + 
				"            <th>Opis</th><th>&nbsp;</th>\r\n" + 
				"        </tr>");
		
		for (Artikal d : artikli) {
			out.println("<tr>");
			out.println("<td>" + d.getNaziv() + "</td>");
			out.println("<td>" + d.getCena() + "</td>");
			out.println("<td>" + d.getMarka()+ "</td>");
			out.println("<td>" + d.getKategorija().getNaziv() + "</td>");
			out.println("<td>" + d.getTip() + "</td>");
			out.println("<td>" + d.getOpis() + "</td>");
			out.println("<td>" + "brisanje" + "</td>");
			out.println("</tr>");
		}
		out.println("</table>");

		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
