package model;

public class Kategorija {

	private int id;
	private String naziv;
	
	public Kategorija(String naziv) {
		super();
		this.naziv = naziv;
	}

	public Kategorija(int id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Kategorija [id=" + id + ", naziv=" + naziv + "]";
	}
	
	
	
}
