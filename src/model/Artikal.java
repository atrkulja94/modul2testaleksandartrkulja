package model;

public class Artikal {

	private int id;
	private String naziv;
	private String marka;
	private String opis;
	private String tip;
	private Kategorija kategorija;
	private double cena;
	
	public Artikal(String naziv, String marka, String opis, String tip, Kategorija kategorija, double cena) {
		super();
		this.naziv = naziv;
		this.marka = marka;
		this.opis = opis;
		this.tip = tip;
		this.kategorija = kategorija;
		this.cena = cena;
	}

	public Artikal(int id, String naziv, String marka, String opis, String tip, Kategorija kategorija, double cena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.marka = marka;
		this.opis = opis;
		this.tip = tip;
		this.kategorija = kategorija;
		this.cena = cena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	@Override
	public String toString() {
		return "Artikal [id=" + id + ", naziv=" + naziv + ", marka=" + marka + ", opis=" + opis + ", tip=" + tip
				+ ", kategorija=" + kategorija + ", cena=" + cena + "]";
	}
	
	
	
	
	
}
